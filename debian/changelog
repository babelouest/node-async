node-async (3.0.1-1) unstable; urgency=medium

  * New upstream version
  * Add node-babel-plugin-add-module-exports as dependency
  * Bump Standards-Version to 4.2.1, no changes needed
  * Change Vcs-* urls to Salsa
  * Updated d/copyright, removed deps/nodeunit.js
  * Add lintian override source-is-missing for
    + support/jsdoc/theme/static/scripts/prettify/lang-css.js line length is
      390 characters (>256)
    + support/jsdoc/theme/static/scripts/prettify/prettify.js line length is
      563 characters (>512)
  * Bumb to debhelper 11

 -- Nicolas Mora <nicolas@babelouest.org>  Fri, 16 Nov 2018 12:20:30 -0500

node-async (0.8.0-3) unstable; urgency=medium

  * Use short-form dh sequencer (not cdbs).
    Stop build-depend on cdbs.
  * Add patch 1001 to modernize testsuite: Replace pre-0.12 Node.js
    process.binding('evals') with require('vm') in testsuite.
  * Run testsuite.
    Build-depend on nodeunit.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 07 Jan 2018 19:14:38 +0100

node-async (0.8.0-2) unstable; urgency=medium

  * Modernize cdbs:
    + Drop upstream-tarball hints: Use gbp import-orig --uscan.
    + Do copyright-check in maintainer script (not during build).
    + Stop build-depend on devscripts.
  * Modernize git-buildpackage config:
    + Avoid git- prefix.
    + Filter any .git* file.
  * Update watch file:
    + Bump file format to version 4.
    + Use github pattern from documentation.
    + Set repacksuffix.
    + Add usage comment.
    + Use substitution strings.
  * Update package relations:
    + Stop fallback-build-depend on yui-compressor: Unneeded since
      squeeze.
    + Fix stop libjs-async bogus dependency on nodejs.
    + Fix have libjs-async recommend javascript-common.
    + Stop build-depend on dh-buildinfo.
  * Stop resolve build-dependencies indirectly in rules file.
  * Modernize Vcs-* fields:
    + Use protocol https (not http or git).
    + Use git (not cgit) in path.
    + Include .git suffix in path.
  * Use package section javascript (not web).
  * Improve short and long descriptions.
  * Declare compliance with Debian Policy 4.1.3.
  * Use dephelper compatibility level 9 (not 8).
  * Fix deduplicate code: Have node-async symlink from and depend on
    libjs-async.
  * Fix install nodejs code into subdirectory.
    Closes: Bug#772835. Thanks to Olliver Schinagl.
  * Tighten rules file: Generalize resolving JavaScript source files.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage for myself.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 07 Jan 2018 18:17:15 +0100

node-async (0.8.0-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Bump standards-version to 3.9.5.
  * Update Vcs-* fields: Packaging git moved to pkg-javascript area of
    Alioth.
  * Update copyright info:
    + Extend coverage for myself.
    + Extend copyright coverage for main upstream author.
  * Fix use TLS in CDBS get-orig-source target, and update URL to not
    use "v" prefix.
  * Stop suppress uglified files from copyright check: No longer
    included in upstream source.
  * Build-depend on devscript (needed for copyright check).
  * Install package.json.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 02 May 2014 10:52:19 +0200

node-async (0.2.5-1) unstable; urgency=low

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Stop tracking md5sum of upstream tarball.
  * Bump packaging license to GPL-3+, and extend copyrigt coverage for
    myself to include current year.
  * Have git-import-orig suppress upstream .gitignore file.
  * List upstream issue tracker as preferred contact.
  * Bump standards-version to 3.9.4.
  * Unset executable bit on library files.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 15 Aug 2013 20:16:45 +0200

node-async (0.1.22-2) unstable; urgency=low

  * Fix build-depend on node-uglify (not libnode-uglify).
    Closes: bug#695814. Thanks to Adam Conrad.
  * Update watch and rules files to directly use github.com URL (not
    githubredir.debian.net).
  * Bump dephelper compatibility level to 8.
  * Update copyright file:
    + Fix use pseudo-comment section to obey silly restrictions of
      copyright format 1.0.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 13 Dec 2012 02:04:24 +0100

node-async (0.1.22-1) unstable; urgency=low

  * New upstream release.
    + Avoid double calling tasks in auto when one or more tasks are
      synchronous.

  * Extend copyright of packaging to cover 2012.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 01 Jun 2012 09:18:55 +0200

node-async (0.1.21-1) unstable; urgency=low

  * New upstream release.
    + Fix pass null (not undef) as error to callbacks, when no error
      occured.
    + Add a ';', to avoid uglify failing with strict_semicolons option.
    + Fix occasional synchronous use of forEachLimit.

  * Use anonscm.debian.org for Vcs-Browser field.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 24 May 2012 16:50:24 +0200

node-async (0.1.18~20120312-1) unstable; urgency=low

  * New upstream (snapshot of) release.
  * Update copyright file:
    + Fix double-indent in Copyright fields as per Policy §5.6.13.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 19 Mar 2012 20:45:00 +0100

node-async (0.1.17-1) unstable; urgency=low

  * New upstream release.
  * Update watch file to use more flexible regex.
  * Bump copyright file format to 1.0.
  * Bump standards-version to 3.9.3.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 01 Mar 2012 20:18:24 +0100

node-async (0.1.15-1) unstable; urgency=low

  * New upstream release.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 23 Dec 2011 15:35:18 +0700

node-async (0.1.15~20111104-1) unstable; urgency=low

  * Initial release.
    Closes: Bug#639095.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 04 Dec 2011 19:48:43 +0700
